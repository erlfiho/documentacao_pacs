CC=latex

all: compile convert 
	@echo "Blz!" 
dirty: compile convert 
	@echo "Blz!" 
compile:
	@latex relatorio.tex
convert:
	@dvipdf relatorio.dvi
view: all
	@evince relatorio.pdf 
clean: 
	@find . -iname *.aux -exec rm -v {} \;
	@find . -iname *.dvi -exec rm -v {} \;
	@find . -iname *.log -exec rm -v {} \;
	@find . -iname *.toc -exec rm -v {} \;
remove: 
	@rm -rfv lixo/*
